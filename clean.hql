-- créée la table
create external table IF NOT EXISTS ytbtable (
        channel_title string,
        title string,
        region string,
        duration string,
        category string,
        comment_count string,
        like_count string,
        view_count string)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ',\t';
--load les datas dans la table
load data local inpath "ytbvids.csv" overwrite into table ytbtable;

--vidéo les plus long en terme de durée
SELECT title, duration, (unix_timestamp(duration ,'mm:ss') - unix_timestamp('00:00:00','HH:mm:ss')) AS RANK from ytbtable ORDER BY RANK desc LIMIT 10;

--vidéo avec le plus long titre
SELECT * from ytbtable ORDER by length(title) desc LIMIT 20;

--category avec le plus de video
SELECT category, COUNT(category) AS RANK from ytbtable WHERE length(category) > 0 GROUP BY category ORDER BY RANK desc LIMIT 10;

--vidéo avec le plus de vue
select title, cast(regexp_extract(view_count, '[0-9]+', 0) as int) as RANK from ytbtable order by RANK desc LIMIT 10;

--vidéo avec le plus de like
select title, cast(regexp_extract(like_count, '[0-9]+', 0) as int) as RANK from ytbtable order by RANK desc LIMIT 10;

--vidéo avec le plus de commentaire
select title, cast(regexp_extract(comment_count, '[0-9]+', 0) as int) as RANK from ytbtable where length(comment_count) > 0 group by title, comment_count order by RANK desc LIMIT 10;

-- chaine youtube avec le plus de vidéo en tendance
select channel_title, Count(channel_title) as RANK from ytbtable group by channel_title order by RANK desc LIMIT 10;

--moyenne de la durée de toute les vidéo en minute
select AVG(unix_timestamp(duration ,'mm:ss') - unix_timestamp('00:00:00','HH:mm:ss'))) from ytbtable;
--il est normal que a la conversion les query ne donne exactement pas les meme résultat car la query minute ne compte pas les second donc les short de youtube

--moyenne de la durée de toute les vidéo en seconde
select AVG((unix_timestamp(duration ,'HH:mm:ss') - unix_timestamp('00:00:00','HH:mm:ss'))) from ytbtable;

--nombre de vidéo supérieur à 21 min
--21 minute étant la moyenne obtenu plus haut
select Count(duration) as RANK from ytbtable WHERE(unix_timestamp(duration ,'HH:mm:ss') - unix_timestamp('00:00:00','HH:mm:ss')) > 21*60;

-- nombre de vidéo inférieur à 21 min
select Count(duration) as RANK from ytbtable WHERE(unix_timestamp(duration ,'HH:mm:ss') - unix_timestamp('00:00:00','HH:mm:ss')) < 21*60;

-- nombre moyen de like fait par les shorts
-- les shorts sont déterminé par les vidéo de moins de 60 sec
select AVG(cast(regexp_extract(like_count, '[0-9]+', 0) as int)) from ytbtable where (unix_timestamp(duration ,'HH:mm:ss') - unix_timestamp('00:00:00','HH:mm:ss')) < 60;

-- nombre moyen de like fait par les vidéos n'étant pas des shorts
select AVG(cast(regexp_extract(like_count, '[0-9]+', 0) as int)) from ytbtable where (unix_timestamp(duration ,'HH:mm:ss') - unix_timestamp('00:00:00','HH:mm:ss')) > 60;
