from src.tuples import cat, regions
from src.auth import youtube_authenticate
from src.ytb import get_video_details, get_video_infos

n_pages = 1000

# récupère les vidéos, éxtrait les données
def extract_ytb_most_popular(youtube, myfilename ):

    for r in regions:
        next_token = None
        for n in range(n_pages):
            request = youtube.videos().list(
                    part="snippet,contentDetails,statistics",
                    chart="mostPopular",
                    regionCode=r[0],
                    maxResults=50,
                    pageToken= next_token
                )
            response = request.execute()

            items = response.get("items")
            for item in items:
                vidid = item["id"]

                video_response = get_video_details(youtube, id=vidid)
                get_video_infos(video_response, r[1], myfilename)

            # passe à la prochaine page si elle existe ou se trouve d'autre vidéo
            if "nextPageToken" in response:
                next_token = response["nextPageToken"]
            else:
                break


def main():

    youtube = youtube_authenticate()
    myfilename = "data/ytbvids.csv"
    extract_ytb_most_popular(youtube, myfilename)


if __name__ == "__main__":
    main()