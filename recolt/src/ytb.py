from src.tuples import cat
import re

listofalltitle = []
# Ecrit un header au fichier csv mais j'ai préféré ne pas l'utilisé.
def write_header(myfilename):
    file = open(myfilename,'a+')
    file.write("channel,\ttitle,\tregion,\tduration,\tcategory,\tcomment_count,\tlike,\tviews\n")
    file.close()

# récupère les détails de la vidéo
def get_video_details(youtube, **kwargs):
    return youtube.videos().list(
        part="snippet,contentDetails,statistics",
        **kwargs
    ).execute()


# fonction récupérant les information et le écrivant dans le fichier
# modifier de https://www.thepythoncode.com/article/using-youtube-api-in-python
def get_video_infos(video_response, region, filename):
    items = video_response.get("items")[0]
    snippet         = items["snippet"]
    statistics      = items["statistics"]
    content_details = items["contentDetails"]
    channel_title = snippet["channelTitle"]
    title         = snippet["title"]
    categoryid = snippet["categoryId"]
    if title in listofalltitle:
        return
    else:
        listofalltitle.append(title)
    try:
        comment_count = statistics["commentCount"]
    except:
        comment_count = ""

    try:
        like_count = statistics["likeCount"]
    except:
        like_count = ""

    try:
        view_count = statistics["viewCount"]
    except:
        view_count = ""
    # get duration from content details
    duration = content_details["duration"]

    # utilisation d'une regex pour bien récupéré la durée
    x = re.findall('[0-9]+', duration)
    # sert a bien annoté quel partie sont des heure, minute et seconde
    if "H" not in duration:
        x.insert(0, "00")

    if "S" not in duration:
        x.append("00")

    if "M" not in duration:
        x.insert(1, "00")

    duration_str = ':'.join(x)

    category_str = "None"
    for i in cat:
        if categoryid == i[0]:
            category_str = i[1]
            break

    file = open(filename,'a+')

    file.write(channel_title + ",\t")
    file.write(title + ",\t")
    file.write(region + ",\t")
    file.write(duration_str + ",\t")
    file.write(category_str + ",\t")
    file.write(comment_count + ",\t")
    file.write(like_count + ",\t")
    file.write(view_count)
    file.write("\n")

    file.close()