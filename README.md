## structure

Le fichier "Installationmanual" contient tout les étapes utilisé pour la réalisation de l'architecture distribué sur Google cloud.
le fichier "ytb.hql" est la ou j'ai écrit les hive query que j'utilise pour mon analyse de données.

le dossier src contient quand à lui les script python utilisé pour la récolte des données.
- script.py contient le main ou est lancée la récolte.
- auth.py contient la fonction utilisé pour l'authentification plus simple a l'api youtube car sinon c'est très redondant de revalidé.
- ytb.py contient tout les fonctions utilisé pour récupéré les données à partir de youtube et l'écriture du csv en output.
- tuples.py contient simplement les tuples utilisé pour match certaines information de youtube.

le dossier data sera l'endroit ou le csv va s'écrire.

Il est nécessaire d'avoir un fichier "creditentials.json" qui contient les clés du projets créée sur l'interface google.
le script vous envera a la première utilisation un lien pour confirmé votre utilisation avec un compte google ensuite il sera sauvegardé jusqu'a ce qu'il expire.
il vous faudra également ajouté un utilsateur à l'api en allant dans la page général du projet et descendre à l'ajout d'utilisateur pour enregistré l'addresse email que vous avez prévu d'utilisé.

## utilisation de la récolte 

pour lancée le programme de récolte il faut avoir créée un projet google, activé l'api et rajouté l'adresse email utilisé au utilisateur.
par la suite lancée le script récoltera automatiquement les vidéos les plus populaire et les enregistrera dans un csv jusqu'a ce que vous n'avez plus de crédit.

## Analyse de données

Les Hive query on été testé que individuellement, mais normalement il n'y a pas de problème à le lancé à l'aide du script hql si vous le désiré.
