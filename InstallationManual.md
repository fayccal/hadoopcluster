# OVERVIEW
this manual is for google cloud multinode cluster setup, other clouds like azure or even vm setup on one machine would require other steps.


## INSTALL JAVA
sudo apt install openjdk-11-jdk


## INSTALL/SETUP hadoop

sudo wget https://dlcdn.apache.org/hadoop/common/hadoop-2.10.2/hadoop-2.10.2.tar.gz
tar xzf hadoop-2.10.2.tar.gz
sudo mv hadoop-2.10.2 /usr/local/hadoop


## EDIT bashrc

sudo nano .bashrc

add this content to it
```
JDK_HOME='/usr/lib/jvm/java-1.11.0-openjdk-amd64'
JAVA_HOME=$JDK_HOME
export JDK_HOME
export JAVA_HOME
HADOOP_HOME='/usr/local/hadoop'
export HADOOP_HOME
export HADOOP_MAPRED_HOME=$HADOOP_HOME
export HADOOP_COMMON_HOME=$HADOOP_HOME
export HADOOP_HDFS_HOME=$HADOOP_HOME
export YARN_HOME=$HADOOP_HOME
export HADOOP_CONF_DIR=$HADOOP_HOME/etc/hadoop
export PATH=$HADOOP_HOME/bin:$HADOOP_HOME/sbin:$PATH
```
source .bashrc

## EDIT /etc/hosts
add adress ip's for every machine in the architecture

ip master
ip slave1
ip slave2

## CREATE SSH KEY ON MASTER

ssh-keygen -t rsa
press enter every time it asks you
cat .ssh/id_rsa.pub
copy the key
In google cloud you have to click on every machines name, go to 'edit' scroll down to 'ssh' and add the new key (if you don't do that you will have to paste the key every time).
try to connect from master to other machines to be sure.


## EDIT XML NEEDED
on master
sudo nano /usr/local/hadoop/etc/hadoop/core-site.xml
paste this in configuration
```xml
<property>
<name>fs.defaultFS</name>
<value>hdfs://master:9000</value>
</property>
```
sudo nano /usr/local/hadoop/etc/hadoop/hdfs-site.xml
```xml
<property>
<name>dfs.namenode.name.dir</name><value>/usr/local/hadoop/data/nameNode</value>
</property>
<property>
<name>dfs.datanode.data.dir</name><value>/usr/local/hadoop/data/dataNode</value>
</property>
<property>
<name>dfs.replication</name>
<value>2</value>
</property>
```

## Edit hadoop-env.sh

sudo nano /usr/local/hadoop/etc/hadoop/hadoop-env.sh

edit line 25 for the java export.
```
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64/
```
## SPECIFICATION OF MASTER AND SLAVE MACHINES
sudo nano masters
paste name of master machine

master

sudo nano slaves
paste slaves machines

slave1
slave2


very usuful command to copy your hadoop/etc/hadoop to every slaves.

scp /usr/local/hadoop/etc/hadoop/* slave1:/usr/local/hadoop/etc/hadoop/
scp /usr/local/hadoop/etc/hadoop/* slave2:/usr/local/hadoop/etc/hadoop/


## FORMAT NAMENODE ON MASTER
hdfs namenode -format

## START
start-dfs.sh

## CHECK
jps

normally you will see namenode on master and datanodes on slaves

## ADD A FIREWALL RULE
go to firewall
add a firewall rule
edit following:
- enter a name
- choose in targets "all instance in the network"
- source ip range "0.0.0.0/0
- protocols and port allow all

create
now go to
{masterip}:50070
you should see your nodes now

## YARN
on slaves
sudo nano /usr/local/hadoop/etc/hadoop/yarn-site.xml
```xml
<property>
<name>yarn.resourcemanager.hostname</name>
<value>master</value>
</property>
```
on master
start-yarn.sh

http://master:8088/cluster

## INSTALL HIVE

sudo wget https://downloads.apache.org/hive/hive-2.3.9/apache-hive-2.3.9-bin.tar.gz
tar zxvf apache-hive-2.3.9-bin.tar.gz apache-hive-2.3.9-bin/
sudo mv apache-hive-2.3.9-bin /usr/local/hive

sudo nano .bashrc
```
export HIVE_HOME=/usr/local/hive
export PATH=$PATH:$HIVE_HOME/bin
export CLASSPATH=$CLASSPATH:/usr/local/Hadoop/lib/*:.
export CLASSPATH=$CLASSPATH:/usr/local/hive/lib/*:.
```
source .bashrc
cd $HIVE_HOME/conf
cp hive-env.sh.template hive-env.sh

add to the end
export HADOOP_HOME=/usr/local/hadoop

cd

BEFORE STARTING HIVE USE THIS COMMAND
```
schematool -initSchema -dbType derby
```
try hive

and now it should all be good.
